/* Copyright 1997 Acorn Computers Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**************************************************************/
/* File:    Find.h                                            */
/*          (C) 2000 Pace Micro Technology PLC                */
/*          All rights reserved                               */
/*                                                            */
/* Purpose: Functions related to the Find dialogue box.       */
/*                                                            */
/* Author:  A.D.Hodgkinson.                                   */
/*                                                            */
/* History: 17-Apr-1997 (ADH): Created.                       */
/*          25-Aug-1997 (ADH): Definitions imported from      */
/*                             TBEvents.h.                    */
/*          13-Mar-1998 (ADH): Working code implemented.      */
/*          10-Apr-2000 (ADH): 64-wide comments adopted.      */
/**************************************************************/

#ifndef Browser_Find__
  #define Browser_Find__

  #include <tboxlibs/toolbox.h>

  /* Find dialogue component IDs */

  #define FindLabel                       0x01600
  #define FindWrit                        0x01601
  #define FindFromStart                   0x01602
  #define FindCaseSensitive               0x01603
  #define FindForwards                    0x01604
  #define FindBackwards                   0x01605
  #define FindCancel                      0x01606
  #define FindOK                          0x01607

  /* Find dialogue event codes */

  #define EFindFromStart                  0x01602
  #define EFindCancel                     0x01606
  #define EFindOK                         0x01607

  #define EFindToBeShown                  0x017ff
  #define EFindHidden                     0x017fe

  /* Function prototypes */

  int  find_to_be_shown (int eventcode, ToolboxEvent * event, IdBlock * idb, void * handle);
  int  find_hidden      (int eventcode, ToolboxEvent * event, IdBlock * idb, void * handle);

  void find_close       (void);

#endif /* Browser_Find__ */
