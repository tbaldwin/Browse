/* Copyright 1999 Element 14 Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**************************************************************/
/* File:    NewAllocs.h                                       */
/*          (C) 2000 Pace Micro Technology PLC                */
/*          All rights reserved                               */
/*                                                            */
/* Purpose: Move malloc() etc. over to NSPRLib's ROSallocs.h  */
/*          functions, provided that current compile time     */
/*          options make this a sensible thing to do.         */
/*                                                            */
/* Author:  A.D.Hodgkinson.                                   */
/*                                                            */
/* History: 18-Nov-1998 (ADH): Created.                       */
/*          07-Apr-2000 (ADH): 64-wide comments adopted.      */
/**************************************************************/

#ifndef Browser_NewAllocs__
  #define Browser_NewAllocs__

  #ifdef JAVASCRIPT
    #ifdef JS_USING_MEMLIB
      #ifndef USE_NSPRLIB_ALLOC_REGISTRATION
        #include "NSPRLib/ROSallocs.h"
        #define malloc  rosallocs_malloc
        #define realloc rosallocs_realloc
        #define calloc  rosallocs_calloc
        #define free    rosallocs_free
      #endif
    #endif
  #endif

#endif /* Browser_NewAllocs__ */
