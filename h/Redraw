/* Copyright 1997 Acorn Computers Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**************************************************************/
/* File:    Redraw.h                                          */
/*          (C) 2000 Pace Micro Technology PLC                */
/*          All rights reserved                               */
/*                                                            */
/* Purpose: Redraw functions for the browser.                 */
/*                                                            */
/* Author:  A.D.Hodgkinson.                                   */
/*                                                            */
/* History: 29-Nov-1996 (ADH): Created.                       */
/*          07-Apr-2000 (ADH): 64-wide comments adopted.      */
/*          11-Feb-2006 (ADH): Replaceable function table     */
/*                             introduced.                    */
/**************************************************************/

#ifndef Browser_Redraw__
  #define Browser_Redraw__

  #include <kernel.h>
  #include <tboxlibs/wimp.h>
  #include <HTMLLib/HTMLLib.h>

  #include "FontManage.h" /* For fm_face */
  #include "RedrawCore.h"

  #include "Global.h"

  /* Definitions */

  #define Redraw_Colour_White       0xffffff00
  #define Redraw_Colour_AlmostWhite 0xeeeeee00
  #define Redraw_Colour_BackGrey    0xdddddd00
  #define Redraw_Colour_WNGrey      0xbbbbbb00
  #define Redraw_Colour_MidGrey     0x99999900
  #define Redraw_Colour_PlinthGrey  0x77777700
  #define Redraw_Colour_Black       0x00000000

  #define Redraw_SelectedBorder_OS  4

  /* Redraw jump table functions */

  _kernel_oserror * redraw_jmptbl_line
                    (
                      const browser_data * restrict b,
                      const BBox         * restrict ends,
                      unsigned int                  colour,

                      size_t             * restrict r_size
                    );

  _kernel_oserror * redraw_jmptbl_rectangle
                    (
                      const browser_data * restrict b,
                      const BBox         * restrict rect,
                      unsigned int                  colour,
                      unsigned int                  border,

                      size_t             * restrict r_size
                    );

  _kernel_oserror * redraw_jmptbl_triangle
                    (
                      const browser_data * restrict b,
                      int                           x1,
                      int                           y1,
                      int                           x2,
                      int                           y2,
                      int                           x3,
                      int                           y3,
                      unsigned int                  colour,

                      size_t             * restrict r_size
                    );

  _kernel_oserror * redraw_jmptbl_circle
                    (
                      const browser_data * restrict b,
                      int                           x,
                      int                           y,
                      unsigned int                  radius,
                      unsigned int                  colour,

                      size_t             * restrict r_size
                    );


  _kernel_oserror * redraw_jmptbl_text
                    (
                      const browser_data * restrict b,
                      fm_face                       handle,
                      int                           x,
                      int                           y,
                      const char         * restrict string,
                      unsigned int                  numbytes,
                      unsigned int                  fgcolour,
                      unsigned int                  bgcolour,
                      unsigned int                  blend,

                      size_t             * restrict r_size
                    );


  _kernel_oserror * redraw_jmptbl_image
                    (
                      const browser_data * restrict b,
                      unsigned int                  image,
                      int                           x,
                      int                           y,
                      unsigned int                  width,
                      unsigned int                  height,

                      size_t             * restrict r_size
                    );

  _kernel_oserror * redraw_jmptbl_sprite
                    (
                      const browser_data * restrict b,
                      const char         * restrict name,
                      int                           x,
                      int                           y,

                      size_t             * restrict r_size
                    );

  _kernel_oserror * redraw_jmptbl_clipping
                    (
                      const browser_data * restrict b,
                      const BBox         * restrict desired,

                      size_t             * restrict r_size
                    );

  /* Other function prototypes */

  unsigned int      redraw_header            (unsigned int flags);

  unsigned int      redraw_backcol           (const browser_data * restrict b);
  void              redraw_set_colour        (unsigned int colour);
  unsigned int      redraw_background_colour (const browser_data * restrict b, const unsigned int foregroundcolour);
  unsigned int      redraw_token_colour      (const browser_data * restrict b, const HStream * restrict t);

  int               redraw_display_width     (const browser_data * restrict b, const reformat_cell * d);
  int               redraw_display_height    (const browser_data * restrict b, const reformat_cell * d);
  int               redraw_left_margin       (const browser_data * restrict b, const reformat_cell * d);
  int               redraw_right_margin      (const browser_data * restrict b, const reformat_cell * d);
  int               redraw_left_gap          (const browser_data * restrict b, const reformat_cell * d, const HStream * restrict t);
  int               redraw_right_gap         (const browser_data * restrict b, const reformat_cell * d, const HStream * restrict t);

  int               redraw_start_x           (const browser_data * restrict b, const reformat_cell * cell, const HStream * restrict t, const unsigned int line);
  int               redraw_token_x           (const browser_data * restrict b, const reformat_cell * cell, const HStream * restrict t, const unsigned int line);
  int               redraw_chunk_x           (const browser_data * restrict b, const reformat_cell * cell, const unsigned int chunk, const unsigned int line);

  unsigned int      redraw_selected          (const browser_data * restrict b, const HStream * restrict t);
  _kernel_oserror * redraw_border_around_box (const browser_data * restrict b, const BBox * box, const unsigned int colour);

  _kernel_oserror * redraw_draw_placeholder  (browser_data * restrict b, const BBox * restrict gfxwin, const BBox * restrict holder, const HStream * restrict token, const char * restrict text);
  _kernel_oserror * redraw_draw              (browser_data * restrict b, WimpRedrawWindowBlock * restrict r, int * restrict nextline, unsigned int noback, const HStream * restrict nocontent);

#endif /* Browser_Redraw__ */
