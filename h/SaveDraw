/* Copyright 1997 Acorn Computers Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**************************************************************/
/* File:    SaveDraw.h                                        */
/*          (C) 2000 Pace Micro Technology PLC                */
/*          All rights reserved                               */
/*                                                            */
/* Purpose: Save a web page as a Draw file.                   */
/*                                                            */
/* Author:  A.D.Hodgkinson, including various functions from  */
/*          original Customer browser by Merlyn Kline.       */
/*                                                            */
/* History: 13-Nov-1997 (ADH): Created.                       */
/*          07-Apr-2000 (ADH): 64-wide comments adopted.      */
/**************************************************************/

#ifndef Browser_SaveDraw__
  #define Browser_SaveDraw__

  #include <kernel.h>

  #include "Global.h"

  /* General bits and pieces */

  typedef enum
  {
    draw_OBJFONTLIST = 0,
    draw_OBJTEXT     = 1,
    draw_OBJPATH     = 2,
    draw_OBJSPRITE   = 5,
    draw_OBJGROUP    = 6,
    draw_OBJTEXTAREA = 9,
    draw_OBJTEXTCOL  = 10,
    draw_OBJTEXTTRFM = 12,
    draw_OBJSPRITRFM = 13,
    draw_OBJJPEG     = 16
  }
  draw_tagtyp;

  typedef int  draw_sizetyp;
  typedef BBox draw_bboxtyp;
  typedef int  draw_coltyp;

  /* For paths */

  typedef int draw_pathwidth;

  /* Use these for path styles described as a 32-bit number */

  #define DRAW_JOINSTYLE_MASK       (3u)
  #define DRAW_JOINSTYLE_MITRED     (0)
  #define DRAW_JOINSTYLE_ROUND      (1u)
  #define DRAW_JOINSTYLE_BEVELLED   (2u)

  #define DRAW_ENDCAP_MASK          (12u)
  #define DRAW_ENDCAP_BUTT          (0)
  #define DRAW_ENDCAP_ROUND         (1u<<2)
  #define DRAW_ENDCAP_SQUARE        (2u<<2)
  #define DRAW_ENDCAP_TRIANGLE      (3u<<2)

  #define DRAW_STARTCAP_MASK        (48u)
  #define DRAW_STARTCAP_BUTT        (0)
  #define DRAW_STARTCAP_ROUND       (1u<<4)
  #define DRAW_STARTCAP_SQUARE      (2u<<4)
  #define DRAW_STARTCAP_TRIANGLE    (3u<<4)

  #define DRAW_WINDRULE_MASK        (64u)
  #define DRAW_WINDRULE_NONZERO     (0)
  #define DRAW_WINDRULE_EVENODD     (1u<<6)

  #define DRAW_DASHFLAG_MASK        (128u)
  #define DRAW_DASHFLAG_NONE        (0)
  #define DRAW_DASHFLAG_PRESENT     (1u<<7)

  #define DRAW_TRIANGLEWIDTH_MASK   (0xff0000u)
  #define DRAW_TRIANGLEWIDTH_SHIFT  (16)
  #define DRAW_TRIANGLELENGTH_MASK  (0xff000000u)
  #define DRAW_TRIANGLELENGTH_SHIFT (24)

  /* Alternatively, break the word down into this structure */

  typedef struct
  {
    unsigned char joincapwind; /* 1 byte  */ /* bit 0..1 join         */
                                             /* bit 2..3 end cap      */
                                             /* bit 4..5 start cap    */
                                             /* bit 6    winding rule */
                                             /* bit 7    dashed       */
    unsigned char reserved8;   /* 1 byte  */
    unsigned char tricapwid;   /* 1 byte  */ /* 1/16th of line width */
    unsigned char tricaphei;   /* 1 byte  */ /* 1/16th of line width */
  }
  draw_pathstyle;

  #define DRAW_TAGID_MASK          (0xffu)

  typedef enum
  {
     draw_PathTERM       = 0, /* end of path                                   */
     draw_PathPTR        = 1, /* pointer to continuation of path               */
     draw_PathMOVE       = 2, /* move to (x,y), starts new subpath             */
     draw_PathMOVENOWIND = 3, /* move to (x,y) without affecting winding       */
     draw_PathCLOSEGAP   = 4, /* close current subpath with a gap              */
     draw_PathCLOSE      = 5, /* close current subpath with a line             */
     draw_PathCURVE      = 6, /* bezier curve to (x3,y3) with 2 control points */
     draw_PathGAP        = 7, /* gap to (x,y) without starting new subpath     */
     draw_PathLINE       = 8  /* line to (x,y)                                 */
  }
  draw_path_tagtype;

  /* For text */

  typedef char draw_fontref;

  typedef struct
  {
    draw_fontref fontref;    /* 1 byte  */
    char         reserved8;  /* 1 byte  */
    short        reserved16; /* 2 bytes */
  }
  draw_textstyle;

  typedef unsigned int draw_fontsize;

  typedef struct
  {
    int         typeface;   /* Index into fontname table */
    int         typesizex;
    int         typesizey;
    draw_coltyp textcolour; /* Text colour RGB */
    draw_coltyp background; /* Hint for anti-aliased printing RGB */
  }
  fontrec;


  /* Path structure header item */

  typedef struct
  {
    draw_tagtyp     tag;         /* 1 word  */
    draw_sizetyp    size;        /* 1 word  */
    draw_bboxtyp    bbox;        /* 4 words */
    draw_coltyp     fillcolour;  /* 1 word  */
    draw_coltyp     pathcolour;  /* 1 word  */
    draw_pathwidth  pathwidth;   /* 1 word  */
    draw_pathstyle  pathstyle;   /* 1 word  */
  }
  draw_pathstrhdr;

  /* Sprite header */

  typedef struct
  {
    draw_tagtyp  tag;  /* 1 word  */
    draw_sizetyp size; /* 1 word  */
    draw_bboxtyp bbox; /* 4 words */
  }
  draw_spristrhdr;

  /* A line of text (maybe transformed) */

  typedef struct
  {
    int x;
    int y;
  }
  draw_objcoord;

  typedef struct
  {
    draw_tagtyp    tag;        /* 1 word  */
    draw_sizetyp   size;       /* 1 word  */
    draw_bboxtyp   bbox;       /* 4 words */
    draw_coltyp    textcolour; /* 1 word  */
    draw_coltyp    background; /* 1 word  */
    draw_textstyle textstyle;  /* 1 word  */
    draw_fontsize  fsizex;     /* 1 word, unsigned */
    draw_fontsize  fsizey;     /* 1 word, unsigned */
    draw_objcoord  coord;      /* 2 words */
  }
  draw_textstrhdr;

  #define SaveDraw_FontFlags_Kerned      (1u<<0)
  #define SaveDraw_FontFlags_RightToLeft (1u<<1)

  typedef struct
  {
    draw_tagtyp    tag;        /* 1 word  */
    draw_sizetyp   size;       /* 1 word  */
    draw_bboxtyp   bbox;       /* 4 words */
    int            matrix[6];
    unsigned int   fontflags;
    draw_coltyp    textcolour; /* 1 word  */
    draw_coltyp    background; /* 1 word  */
    draw_textstyle textstyle;  /* 1 word  */
    draw_fontsize  fsizex;     /* 1 word, unsigned */
    draw_fontsize  fsizey;     /* 1 word, unsigned */
    draw_objcoord  coord;      /* 2 words */
  }
  draw_trfmtextstrhdr;

  /* File header */

  typedef struct
  {
    char         title[4];

    int          majorstamp;
    int          minorstamp;
    char         progident[12];

    draw_bboxtyp bbox;
  }
  draw_fileheader;

  /* General header for graphic objects */

  typedef struct
  {
    draw_tagtyp  tag;  /* 1 word  */
    draw_sizetyp size; /* 1 word  */
    draw_bboxtyp bbox; /* 4 words */
  }
  draw_objhdr;

  /* A font list */

  typedef struct
  {
    draw_tagtyp  tag;  /* 1 word  */
    draw_sizetyp size; /* 1 word  */
  }
  draw_fontliststrhdr;

  typedef struct
  {
    draw_tagtyp  tag;
    draw_sizetyp size;

    draw_fontref fontref;
    char         fontname[1]; /* String, null terminated */
  }
  draw_fontliststr;

  /* For sprites */

  typedef struct /* Format of a sprite header */
  {
    int  next;      /*  Offset to next sprite                */
    char name[12];  /*  Sprite name                          */
    int  width;     /*  Width in words-1      (0..639)       */
    int  height;    /*  Height in scanlines-1 (0..255/511)   */
    int  lbit;      /*  First bit used (left end of row)     */
    int  rbit;      /*  Last bit used (right end of row)     */
    int  image;     /*  Offset to sprite image               */
    int  mask;      /*  Offset to transparency mask          */
    int  mode;      /*  Mode sprite was defined in           */
                    /*  Palette data optionally follows this */
                    /*  in memory                            */
  }
  sprite_header;

  typedef struct
  {
   int width;
   int height;
   int mask;
   int mode;
  }
  sprite_info;

  /* We're going to fix scale factors for OS units and points */
  /* to Draw units.                                           */

  #define PTD(x) (((x)*16)/25) /* Convert millipoints to draw units */
  #define OTD(x) ((x)*256)     /* Convert OS units to draw units    */

  /* Some miscellaneous useful definitions */

  #define DSIZE_FLINE (sizeof(draw_pathstrhdr)+2*12+4)
  #define DSIZE_FRECT (sizeof(draw_pathstrhdr)+5*12+4)
  #define DSIZE_FTRIA (sizeof(draw_pathstrhdr)+4*12+4)

  /* Function prototypes */

  _kernel_oserror * savedraw_write_bytes    (const char * restrict s, size_t n);

  _kernel_oserror * savedraw_save_draw      (browser_data * b, const char * pathname, int bgimages);
  unsigned int      savedraw_draw_size      (browser_data * b, int bgimages);

#endif /* Browser_SaveDraw__ */
