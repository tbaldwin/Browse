/* Copyright 1997 Acorn Computers Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**************************************************************/
/* File:    FetchPage.h                                       */
/*          (C) 2000 Pace Micro Technology PLC                */
/*          All rights reserved                               */
/*                                                            */
/* Purpose: High-level fetch functions; the main interface    */
/*          for initiating and controlling full page fetches. */
/*          Compare with lower level Fetch.h and FetchHTML.h. */
/*                                                            */
/* Author:  A.D.Hodgkinson.                                   */
/*                                                            */
/* History: 25-Nov-1996 (ADH): Created.                       */
/*          10-Apr-2000 (ADH): 64-wide comments adopted.      */
/**************************************************************/

#ifndef Browser_FetchPage__
  #define Browser_FetchPage__

  #include <kernel.h>

  #include "Global.h"

  /* Function prototypes */

  int               fetchpage_fetch           (int eventcode, WimpPollBlock * b, IdBlock * idb, browser_data * handle);
  _kernel_oserror * fetchpage_fetch_targetted (browser_data * parent, const char * url, const char * target, const char * appnddata, int new_window, int no_anchor_errors);

  _kernel_oserror * fetchpage_postprocess_uri (browser_data * b, char * uri, int record);

  _kernel_oserror * fetchpage_new             (browser_data * b, const char * url, int record, int stop, int no_anchor_errors);
  _kernel_oserror * fetchpage_new_add         (browser_data * b, const char * url, int record, int stop, int no_anchor_errors, const char * add, int new_window, const char * name);
  _kernel_oserror * fetchpage_new_raw         (browser_data * b, const char * url, int record, int stop, int no_anchor_errors);

  void              fetchpage_claim_nulls     (browser_data * b);
  void              fetchpage_release_nulls   (browser_data * b);

#endif /* Browser_FetchPage__ */
