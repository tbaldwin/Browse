/* Copyright 1998 Acorn Computers Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**************************************************************/
/* File:    ItemInfo.h                                        */
/*          (C) 2000 Pace Micro Technology PLC                */
/*          All rights reserved                               */
/*                                                            */
/* Purpose: Functions related to the Item Information window. */
/*                                                            */
/* Author:  A.D.Hodgkinson.                                   */
/*                                                            */
/* History: 11-Mar-1998 (ADH): Created.                       */
/*          10-Apr-2000 (ADH): 64-wide comments adopted.      */
/**************************************************************/

#ifndef Browser_ItemInfo__
  #define Browser_ItemInfo__

  #include <kernel.h>
  #include <tboxlibs/toolbox.h>
  #include <HTMLLib/HTMLLib.h>

  #include "Global.h"

  /* Item Info dialogue component IDs */

  #define IIOK                            0x02800
  #define IIFollowLink                    0x02811
  #define IIExportItem                    0x02812

  #define IIItemTypeLabel                 0x02801
  #define IIItemTypeDisplay               0x02802
  #define IILinkToLabel                   0x02803
  #define IILinkToDisplay                 0x02804
  #define IIFetchedFromLabel              0x02805
  #define IIFetchedFromDisplay            0x02806

  #define IIActualSizeMainLabel           0x02807
  #define IIActualSizeWidthDisplay        0x02808
  #define IIActualSizeMiddleLabel         0x02809
  #define IIActualSizeHeightDisplay       0x0280a
  #define IIActualSizeEndLabel            0x0280b

  #define IIScaledToMainLabel             0x0280c
  #define IIScaledToWidthDisplay          0x0280d
  #define IIScaledToMiddleLabel           0x0280e
  #define IIScaledToHeightDisplay         0x0280f
  #define IIScaledToEndLabel              0x02810

  /* Item Info dialogue event codes */

  #define EIIOK                           0x02800
  #define EIIFollowLink                   0x02811
  #define EIIExportItem                   0x02812

  #define EIIToBeShown                    0x029ff

  /* Function prototypes */

  int               iteminfo_to_be_shown      (int eventcode, ToolboxEvent * event, IdBlock * idb, void * handle);
  int               iteminfo_follow_link      (int eventcode, ToolboxEvent * event, IdBlock * idb, void * handle);
  int               iteminfo_export_item      (int eventcode, ToolboxEvent * event, IdBlock * idb, void * handle);

  _kernel_oserror * iteminfo_fill_in_contents (browser_data * b, HStream * t, ObjectId o);

#endif /* Browser_ItemInfo__ */
