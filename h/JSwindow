/* Copyright 1998 Acorn Computers Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**************************************************************/
/* File:    JSwindow.h                                        */
/*          (C) 2000 Pace Micro Technology PLC                */
/*          All rights reserved                               */
/*                                                            */
/* Purpose: JavaScript support - Window objects. Based in     */
/*          part on source in libmocha from the Mozilla       */
/*          browser sources.                                  */
/*                                                            */
/* Author:  A.D.Hodgkinson.                                   */
/*                                                            */
/* History: 06-May-1998 (ADH): Created.                       */
/*          07-Apr-2000 (ADH): 64-wide comments adopted.      */
/**************************************************************/

#ifndef Browser_JSwindow__
  #define Browser_JSwindow__

  #ifdef JAVASCRIPT

    #include <JSLib/JSLibAPI.h>

    #include "Global.h"

    JSObject * jswindow_new_window                (browser_data * b);
    void       jswindow_destroy_window            (browser_data * b);

    JSBool     jswindow_resolve_window_properties (browser_data * decoder, JSObject * obj, jsval id);

  #endif

#endif /* Browser_JSwindow__ */
