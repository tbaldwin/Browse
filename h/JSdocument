/* Copyright 1998 Acorn Computers Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**************************************************************/
/* File:    JSdocument.h                                      */
/*          (C) 2000 Pace Micro Technology PLC                */
/*          All rights reserved                               */
/*                                                            */
/* Purpose: JavaScript support - Document objects. Based in   */
/*          part on source in libmocha from the Mozilla       */
/*          browser sources.                                  */
/*                                                            */
/* Author:  A.D.Hodgkinson.                                   */
/*                                                            */
/* History: 07-May-1998 (ADH): Created.                       */
/*          07-Apr-2000 (ADH): 64-wide comments adopted.      */
/**************************************************************/

#ifndef Browser_JSdocument__
  #define Browser_JSdocument__

  #define JSDocument_ZeroPointGMTDate "Thu, 01 Jan 1970 00:00:00 GMT"

  #ifdef JAVASCRIPT

    #include <JSLib/JSLibAPI.h>

    #include "Global.h"

    /* Note that struct JSDocument is described in JSconsts.h */

    JSBool     jsdocument_initialise_class    (browser_data * decoder);
    JSObject * jsdocument_define_document     (browser_data * decoder);

  #endif

#endif /* Browser_JSdocument__ */
