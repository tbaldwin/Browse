/* Copyright 2006 Castle Technology Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**************************************************************/
/* File:    Scale.h                                           */
/*          (C) 2000 Pace Micro Technology PLC                */
/*          All rights reserved                               */
/*                                                            */
/* Purpose: Handling scaled unit conversions. 64-bit integer  */
/*          internal precision, 32-bit integers externally.   */
/*                                                            */
/* Author:  A.D.Hodgkinson.                                   */
/*                                                            */
/* History: 12-Feb-2006 (ADH): Created.                       */
/**************************************************************/

#ifndef Browser_Scale__
  #define Browser_Scale__

  #include <tboxlibs/wimp.h>

  #include "Global.h"

  /* Limits */

  #define SCALE_MIN     5
  #define SCALE_MAX     1000
  #define SCALE_DEFAULT 100

  /* Function prototypes: General */

  void scale_calculate_scale_factors (browser_data * restrict b);

  /* Function prototypes: Converting single numbers */

  int  scale_user_to_os               (const browser_data * restrict b, const int value);
  int  scale_user_to_web              (const browser_data * restrict b, const int value);
  int  scale_user_to_millipoints      (const browser_data * restrict b, const int value);
  int  scale_user_to_pdf              (const browser_data * restrict b, const int value);
  int  scale_user_to_draw             (const browser_data * restrict b, const int value);
  int  scale_os_to_user               (const browser_data * restrict b, const int value);
  int  scale_web_to_user              (const browser_data * restrict b, const int value);
  int  scale_millipoints_to_user      (const browser_data * restrict b, const int value);
  int  scale_pdf_to_user              (const browser_data * restrict b, const int value);
  int  scale_draw_to_user             (const browser_data * restrict b, const int value);

  /* Function prototypes: Converting coordinate pairs */

  void scale_pair_user_to_os          (const browser_data * restrict b, const int x, const int y, int * restrict r_x, int * restrict r_y);
  void scale_pair_user_to_web         (const browser_data * restrict b, const int x, const int y, int * restrict r_x, int * restrict r_y);
  void scale_pair_user_to_millipoints (const browser_data * restrict b, const int x, const int y, int * restrict r_x, int * restrict r_y);
  void scale_pair_user_to_pdf         (const browser_data * restrict b, const int x, const int y, int * restrict r_x, int * restrict r_y);
  void scale_pair_user_to_draw        (const browser_data * restrict b, const int x, const int y, int * restrict r_x, int * restrict r_y);
  void scale_pair_os_to_user          (const browser_data * restrict b, const int x, const int y, int * restrict r_x, int * restrict r_y);
  void scale_pair_web_to_user         (const browser_data * restrict b, const int x, const int y, int * restrict r_x, int * restrict r_y);
  void scale_pair_millipoints_to_user (const browser_data * restrict b, const int x, const int y, int * restrict r_x, int * restrict r_y);
  void scale_pair_pdf_to_user         (const browser_data * restrict b, const int x, const int y, int * restrict r_x, int * restrict r_y);
  void scale_pair_draw_to_user        (const browser_data * restrict b, const int x, const int y, int * restrict r_x, int * restrict r_y);

  /* Function prototypes: Converting bounding boxes */

  void scale_box_user_to_os           (const browser_data * restrict b, const BBox * restrict box, BBox * restrict r_box);
  void scale_box_user_to_web          (const browser_data * restrict b, const BBox * restrict box, BBox * restrict r_box);
  void scale_box_user_to_millipoints  (const browser_data * restrict b, const BBox * restrict box, BBox * restrict r_box);
  void scale_box_user_to_pdf          (const browser_data * restrict b, const BBox * restrict box, BBox * restrict r_box);
  void scale_box_user_to_draw         (const browser_data * restrict b, const BBox * restrict box, BBox * restrict r_box);
  void scale_box_os_to_user           (const browser_data * restrict b, const BBox * restrict box, BBox * restrict r_box);
  void scale_box_web_to_user          (const browser_data * restrict b, const BBox * restrict box, BBox * restrict r_box);
  void scale_box_millipoints_to_user  (const browser_data * restrict b, const BBox * restrict box, BBox * restrict r_box);
  void scale_box_pdf_to_user          (const browser_data * restrict b, const BBox * restrict box, BBox * restrict r_box);
  void scale_box_draw_to_user         (const browser_data * restrict b, const BBox * restrict box, BBox * restrict r_box);

#endif /* Browser_Scale__ */
